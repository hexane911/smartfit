import React from 'react';
import logo from './logo.svg';
import './app/styles/global.sass'
import Layout from './app/components/layout';

function App() {
  return (
    <div className="App">
      <Layout/>
    </div>
  );
}

export default App;
