import { useState } from "react";
import Button from "../components/button";
import Carousel from "../components/carousel";
import Filter from "../components/filter";
import TrainerSlide from "../components/slides/trainer.slide";
import { useWindowSize } from "../hooks";
import { SAMPLE_TRAINERS } from "../temp/data";
import "./trainers.page.sass";

const sample_options = [
  "Все",
  "Похудение",
  "Гибкость",
  "Растяжка",
  "Здоровье",
  "Зарядки",
  "Новичкам",
  "Силовые",
  "Для мам",
  "Йога и медитация",
  "Гантели и резинки",
  "Боли в спине",
  "Ноги и ягодицы",
  "Тонус мышц",
  "Пресс",
  "Комплексные",
  "Для детей",
  "Танцевальные",
];

const TrainersPage = () => {
  const [filter, setFilter] = useState<string>(sample_options[0]);
  const defaultNum = 16;
  const [left, setLeft] = useState(SAMPLE_TRAINERS.length - defaultNum);
  const [limit, setLimit] = useState(defaultNum);

  const { width } = useWindowSize();
  const handleLimit = () => {
    if (left) {
      setLeft((left) => left - defaultNum);
      setLimit((l) => l + defaultNum);
    }
  };
  return (
    <div className="trainers page">
      <div className="trainers__inner wrapper">
        <h2 className="trainers__title center">Наши тренеры</h2>
        <p className="trainers__subtitle _18 center">
          Занимайтесь под руководством профессионалов
        </p>
        <Carousel className="trainers__carousel-1">
          {SAMPLE_TRAINERS.map((el) => (
            <TrainerSlide {...el} />
          ))}
        </Carousel>
        <h3 className="trainers__topic center">Популярные</h3>
        <Carousel className="trainers__carousel-2">
          {SAMPLE_TRAINERS.map((el) => (
            <TrainerSlide {...el} />
          ))}
        </Carousel>
        <h2 className="trainers__title trainers__title-2">По направлениям</h2>
        <Filter
          options={sample_options}
          handler={setFilter}
          current={filter}
          deselectedState={sample_options[0]}
          className={"trainers__filter"}
        />
        {width > 419 ? (
          <>
            <div className="trainers__box">
              {SAMPLE_TRAINERS.map((el, i) =>
                i < limit ? <TrainerSlide {...el} /> : null
              )}
            </div>
            {left > 0 && (
              <Button classname="trainers__button" onClick={handleLimit}>
                Показать ещё
              </Button>
            )}
          </>
        ) : (
          <Carousel className="trainers-mobile">
            {SAMPLE_TRAINERS.map((el, i) => (
              <TrainerSlide {...el} />
            ))}
          </Carousel>
        )}
      </div>
    </div>
  );
};

export default TrainersPage;
