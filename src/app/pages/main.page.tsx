import "./main.page.sass";
import Button from "../components/button";
import Questions from "./questions.section";
import { SAMPLE_BLOG, SAMPLE_PROGRAMS, SAMPLE_QUESTIONS, SAMPLE_REVIEWS, SAMPLE_TRAINERS } from "../temp/data";

// hero section
import check from "../assets/images/check.svg";
import heroimage from "../assets/images/hero.png";

// why section
import why_1Icon from "../assets/images/why_1.svg";
import why_2Icon from "../assets/images/why_2.svg";
import why_3Icon from "../assets/images/why_3.svg";
import why_4Icon from "../assets/images/why_4.svg";
import why_5Icon from "../assets/images/why_5.svg";
import why_6Icon from "../assets/images/why_6.svg";

//connect section
import appleIcon from '../assets/images/appstore.svg'
import googleIcon from '../assets/images/google-play.svg'
import phoneImage from '../assets/images/phone.png'

//special section
import specialImage from '../assets/images/special.png'

import { Question } from "../types/question";
import VideoSection from "./video.section";
import BlogSlide from "../components/slides/blog.slide";
import Carousel from "../components/carousel";
import ProgramSlide from "../components/slides/program.slide";
import TrainerSlide from "../components/slides/trainer.slide";
import Reviews from "./reviews.section";



const MainPage = () => {
  console.log(SAMPLE_BLOG.map(el => {
    return <BlogSlide {...el} />
}))
  return (
    <div className="main-page page main">
      <div className="inner wrapper">
        <div className="hero">
          <div className="hero__left">
            <p className="hero__subtitle">Фитнес от SmartFit</p>
            <h1 className="hero__title">
              Ваше здоровое тело - <br /> наша{" "}
              <span className="underline">общая</span> цель
            </h1>
            <div className="hero__features">
              <div className="hero__item">
                <img src={check} alt="" className="hero__item-icon" />
                Доступ к платформе с любого устройства
              </div>
              <div className="hero__item">
                <img src={check} alt="" className="hero__item-icon" />
                Более 20 готовых программ тренировок
              </div>
              <div className="hero__item">
                <img src={check} alt="" className="hero__item-icon" />
                Новые тренировки каждый месяц
              </div>
            </div>
            <Button onClick={() => {}} icon='bolt' classname="hero__button">
              Начать тренировку
            </Button>
          </div>
          <div className="hero__right">
            <img src={heroimage} alt="" className="hero__image" />
            <Button onClick={() => {}} icon='bolt' classname="hero__button mobile">
              Начать тренировку
            </Button>
          </div>
        </div>

        <div className="programs">
          <h2 className="programs__title">
            Выберите программу, которая подходит именно{" "}
            <span className="underline small">вам</span>
          </h2>
          <p className="programs__subtitle _18 center">
            Ваши личные цели требуют индивидуального подхода. <br /> Именно
            поэтому вы сами выбираете свой путь!
          </p>
          <div className="programs__slider">
            <Carousel className="main-programs">
              {SAMPLE_PROGRAMS.map(el => {
                  return <ProgramSlide {...el} />
              })}
            </Carousel>
          </div>
        </div>

        <div className="why">
          <h2 className="why__title left">
            Почему SmartFit вам{" "}
            <span className="underline small">подходит</span>?
          </h2>
          <div className="why__box">
            <div className="why__item">
              <img src={why_1Icon} alt="" className="why__item-image" />
              <p className="why__item-title">Тренировки в онлайн-формате</p>
              <p className="why__item-desc _16">
                Вы не тратите своё время на лишнее. Занимайтесь онлайн когда
                удобно вам!
              </p>
            </div>
            <div className="why__item">
              <img src={why_2Icon} alt="" className="why__item-image" />
              <p className="why__item-title">Не нужен специальный инвентарь</p>
              <p className="why__item-desc _16">
                Упражнения не требуют покупку особых снарядов. Вам нужно только
                желание.
              </p>
            </div>
            <div className="why__item">
              <img src={why_3Icon} alt="" className="why__item-image" />
              <p className="why__item-title">Экономия и эффективность</p>
              <p className="why__item-desc _16">
                Подписка значительно дешевле, чем покупка абонемента и занятия с
                тренером.
              </p>
            </div>
            <div className="why__item">
              <img src={why_4Icon} alt="" className="why__item-image" />
              <p className="why__item-title">Доступ сразу ко всем программам</p>
              <p className="why__item-desc _16">
                Оформляя подписку, вы получаете доступ сразу ко всем программам
                на сайте.
              </p>
            </div>
            <div className="why__item">
              <img src={why_5Icon} alt="" className="why__item-image" />
              <p className="why__item-title">Для любого уровня подготовки</p>
              <p className="why__item-desc _16">
                Программы тренировок составлены как для новичков, так и для
                людей с опытом.
              </p>
            </div>
            <div className="why__item">
              <img src={why_6Icon} alt="" className="why__item-image" />
              <p className="why__item-title">Готовые программы питания</p>
              <p className="why__item-desc _16">
                Вместе с видеоуроками вы получаете рекомендации по вашему
                рациону.
              </p>
            </div>
          </div>
        </div>

        <div className="connect">
            <div className="connect__phone"></div>
          <div className="connect__box">
            <h2 className="connect__title left">
              Подключайте <br />
              к платформе <br />
              свои устройства <br />
              для удобства <br />
            </h2>
            <p className="_18 connect__desc">
            Установите на свое устройство приложение <br /> и тренируйтесь с нами в любом месте
            </p>
            <div className="connect__download">
              <a href="" className="connect__download-link">
                <img src={appleIcon} alt="" className="connect__download-img" />
              </a>
              <a href="" className="connect__download-link">
                <img src={googleIcon} alt="" className="connect__download-img" />
              </a>
            </div>

          </div>
        </div>

        <div className="main-blog">
          <h2 className="main-blog__title">
          Прокачайте свои <br /> знания на нашем <span className="underline small">блоге</span>
          </h2>
          <p className="_18 main-blog__subtitle center">
          Начните лучше понимать своё тело
          </p>
          <div className="main-blog__box">
            <Carousel className="main-blog__slider" noArrows>
            {SAMPLE_BLOG.map((el, i) => {
              return i > 5 ? null : <BlogSlide className="main-blog__slide" {...el} />
            })}
            </Carousel>
          </div>
          <Button classname="main-blog__button" icon="bolt" onClick={() => {}}>начать тренировку</Button>
        </div>


        <VideoSection/>

        <div className="main-trainers">
          <h2 className="main-trainers__title">
          Наша команда <span className="underline small">экспертов</span>
          </h2>
          <p className="main-trainers__subtitle _18 center">
            Занимайтесь под руководством профессионалов
          </p>
          <div className="main-trainers__box">
            <Carousel noArrows className="main-trainers__slider">
            {SAMPLE_TRAINERS.map((el, i) => {
              return i <= 5 ? <TrainerSlide {...el}/> : null
            })}
            </Carousel>
          </div>
        </div>

        <div className="special">
          <img src={specialImage} alt="" className="special__image" />
          <div className="special__inner">
            <h2 className="special__title left">
            Тренировки, подобранные <br /> специально для вас!
            </h2>
            <p className="special__subtitle _16 left">
            Оформите подписку и тренируйтесь когда удобно <br /> и где удобно: дома, в отпуске, в командировке.
            </p>
            <div className="special__buttons">
              <Button classname="special__button" icon="bolt" onClick={() =>{}}>Тренировки</Button>
              <Button classname="special__button clear" icon="search" clear onClick={() =>{}}>Подробнее</Button>
            </div>
          </div>
        </div>

        

      </div>
      <Reviews props={SAMPLE_REVIEWS}/>
      <Questions props={SAMPLE_QUESTIONS} />
    </div>
  );
};

export default MainPage;
