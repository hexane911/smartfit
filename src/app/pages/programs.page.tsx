import { useState } from "react";
import Button from "../components/button";
import Carousel from "../components/carousel";
import Filter from "../components/filter";
import ProgramSlide from "../components/slides/program.slide";
import { useWindowSize } from "../hooks";
import {
  SAMPLE_PROGRAMS,
  SAMPLE_QUESTIONS,
  SAMPLE_REVIEWS,
} from "../temp/data";
import "./programs.page.sass";
import Questions from "./questions.section";
import Reviews from "./reviews.section";

const sample_options_1 = ["Похудение", "Гибкость", "Здоровье", "Сила"];

const sample_options_2 = [
  "Все",
  "Похудение",
  "Гибкость",
  "Растяжка",
  "Здоровье",
  "Зарядки",
  "Новичкам",
  "Силовые",
  "Для мам",
  "Йога и медитация",
  "Гантели и резинки",
  "Боли в спине",
  "Ноги и ягодицы",
  "Тонус мышц",
  "Пресс",
  "Комплексные",
  "Для детей",
  "Танцевальные",
];

const ProgramsPage = () => {
  const [filter1, setFilter1] = useState<string>("");
  const [filter2, setFilter2] = useState<string>(sample_options_2[0]);
  const defaultNum = 16;
  const [left, setLeft] = useState(SAMPLE_PROGRAMS.length - defaultNum);
  const [limit, setLimit] = useState(defaultNum);

  const { width } = useWindowSize();

  const handleLimit = () => {
    if (left) {
      setLeft((left) => left - defaultNum);
      setLimit((l) => l + defaultNum);
    }
  };
  return (
    <div className="programs page">
      <div className="programs__inner wrapper">
        <h2 className="programs__title center">Программы тренировок</h2>
        <p className="programs__subtitle center _18">
          Всё что тебе нужно для красивого тела на одной странице!
        </p>
        <h3 className="programs__topic">Выберите свою цель:</h3>
        <Filter
          handler={setFilter1}
          current={filter1}
          center
          className="programs__filter programs__filter-1"
          options={sample_options_1}
        />
        <Carousel className="programs__carousel-first">
          {SAMPLE_PROGRAMS.map((el) => {
            return <ProgramSlide {...el} />;
          })}
        </Carousel>
        <h3 className="programs__topic programs__topic-bm">Новинки</h3>
        <Carousel className="programs__carousel-new">
          {SAMPLE_PROGRAMS.map((el) => {
            return <ProgramSlide {...el} />;
          })}
        </Carousel>
        <h3 className="programs__topic programs__topic-bm">Популярные</h3>
        <Carousel className="programs__carousel-popular">
          {SAMPLE_PROGRAMS.map((el) => {
            return <ProgramSlide {...el} />;
          })}
        </Carousel>
        <h2 className="programs__title programs__title-2">По направлениям</h2>
        <Filter
          handler={setFilter2}
          current={filter2}
          deselectedState={sample_options_2[0]}
          className="programs__filter programs__filter-2"
          options={sample_options_2}
        />
        {width > 419 ? (
          <>
            <div className="programs__box">
              {SAMPLE_PROGRAMS.map((el, i) =>
                i < limit ? <ProgramSlide {...el} /> : null
              )}
            </div>
            {left > 0 && (
              <Button classname="programs__button" onClick={handleLimit}>
                Показать ещё
              </Button>
            )}
          </>
        ) : (
          <Carousel noArrows className="programs-mobile">
            {SAMPLE_PROGRAMS.map((el, i) => (
              <ProgramSlide {...el} />
            ))}
          </Carousel>
        )}
      </div>
      <Reviews props={SAMPLE_REVIEWS} />
      <Questions props={SAMPLE_QUESTIONS} />
    </div>
  );
};

export default ProgramsPage;
