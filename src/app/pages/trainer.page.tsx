import Button from "../components/button";
import "./trainer.page.sass";
import outline from "../assets/images/outline.svg";
import example from "../assets/images/dmitri.png";
import Reviews from "./reviews.section";
import {
  SAMPLE_PROGRAMS,
  SAMPLE_QUESTIONS,
  SAMPLE_REVIEWS,
  SAMPLE_TRAINERS,
} from "../temp/data";
import Questions from "./questions.section";
import Carousel from "../components/carousel";
import TrainerSlide from "../components/slides/trainer.slide";
import "./video.section.sass";
import ProgramSlide from "../components/slides/program.slide";

const TrainerPage = () => {
  return (
    <div className="trainer">
      <div className="trainer__upper">
        <div className="trainer__inner h wrapper">
          <div className="trainer__left">
            <p className="trainer__subtitle">Тренер</p>
            <h1 className="trainer__title">Дмитрий Васильев</h1>
            <p className="trainer__info">
              <span>12</span>Программ
            </p>
            <p className="trainer__info">
              <span>150</span>Тренировок
            </p>
            <p className="trainer__info">
              <span>15671</span>Человек занимается
            </p>
            <Button classname="trainer__button" icon="bolt" onClick={() => {}}>
              начать тренировку
            </Button>
          </div>
          <div className="trainer__frame">
            <img src={outline} alt="" className="trainer__background" />
            <img src={example} alt="" className="trainer__image" />
          </div>
          <Button classname="trainer__button mobile" icon="bolt" onClick={() => {}}>
              начать тренировку
            </Button>
        </div>
      </div>
      <div className="trainer__inner wrapper">
        <h2 className="center trainer__h2 m">О <span className="underline">тренере</span></h2>
        <div className="trainer__about video">
          <div className="video__left">
            <Button icon="video" onClick={() => {}} classname="video__button">
              видео о тренере
            </Button>
          </div>
          <div className="video__right">
            <p className="_18 video__desc">
              Вы давно мечтаете похудеть, нарастить мышечную массу или
              сформировать красивый рельеф? С платформой SmartFit вам откроются
              новые возможности онлайн-фитнеса с гарантированным результатом!
              <br /><br />
              Наша команда тренеров с многолетним опытом подготовила программы
              видеотренировок для различных целей: похудение, набор массы,
              рельеф, растяжка и многое другое.
              <br /><br />
               Ваш результат начинается со
              SmartFit! Создайте тело мечты!
            </p>
          </div>
        </div>
        <h2 className="trainer__h2 center">
          Программы с <span className="underline small">тренером</span>
        </h2>
        <p className="_18 trainer__p center">
        Выберите то что подходит именно вам!
        </p>
        <Carousel className="trainer__programs" noArrows>
          {SAMPLE_PROGRAMS.map((el) => (
            <ProgramSlide {...el} />
          ))}
        </Carousel>
        <h2 className="trainer__h2 center">
          Другие <span className="underline small">тренера</span>
        </h2>
        <p className="_18 trainer__p center">
          Занимайтесь под руководством профессионалов
        </p>
        <Carousel className="trainer__others" noArrows>
          {SAMPLE_TRAINERS.map((el) => (
            <TrainerSlide {...el} />
          ))}
        </Carousel>
      </div>
      <Reviews props={SAMPLE_REVIEWS} />
      <Questions props={SAMPLE_QUESTIONS} />
    </div>
  );
};

export default TrainerPage;
 