import { SAMPLE_PROGRAMS } from '../../temp/data'
import './lk-programs.sass'
import ProgramItem from './program.item'

const LK_Programs = () => {
    return (
        <div className="lk-programs">
            {SAMPLE_PROGRAMS.map((el, i) => {
                return <ProgramItem {...el} />
            })}
        </div>
    )
}

export default LK_Programs