import { Outlet, Route, Routes } from 'react-router-dom'
import LogoutModal from '../../components/modals/logout.modal'
import './profile.sass'
import Sidebar from './sidebar'


const Profile = () => {
    
    return <div className="profile">
            <Sidebar />
        <div className="profile__inner">
            <div className="profile__wrapper">
                <Outlet />
            </div>
        </div>
    </div>
}

export default Profile