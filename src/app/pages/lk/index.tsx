import "./index.sass";
import trophyIcon from "../../assets/images/example_trophy.svg";
import daysIcon from "../../assets/images/days.svg";
import picture from "../../assets/images/picture.png";

const ProfileIndex = () => {
  return (
    <div className="profile-index">
      <div className="profile-index__box">
        <div className="profile-index__image">D</div>
        <p className="profile-index__name">Dima.vasiliev.98</p>
      </div>
      <div className="profile__line" />
      <div className="profile-index__trophies">
        <img src={trophyIcon} alt="" className="profile-index__trophy" />
        <img src={trophyIcon} alt="" className="profile-index__trophy" />
        <img src={trophyIcon} alt="" className="profile-index__trophy" />
        <img src={trophyIcon} alt="" className="profile-index__trophy" />
        <img src={trophyIcon} alt="" className="profile-index__trophy" />
        <img src={trophyIcon} alt="" className="profile-index__trophy" />
      </div>
      <div className="profile-index__sides">
        <div className="profile-index__feed">
          <h3 className="prof">Лента</h3>
          <div className="profile-index__feed-items">
            <div className="profile-index__feed-item">
              <img src={picture} alt="" className="profile-index__feed-image" />
              <div>
                <p className="profile-index__feed-name">
                  Dima.vasiliev.98
                  <span className="profile-index__feed-date">
                    2 месяца назад
                  </span>
                </p>
                <p className="profile-index__item-desc">
                  Выполнил(а) тренировку{" "}
                  <a href="" className="profile-index__feed-link">
                    "Пресс 2/30"
                  </a>{" "}
                  по программе{" "}
                  <a href="" className="profile-index__feed-link">
                    "Стальной пресс"
                  </a>
                </p>
              </div>
            </div>
            <div className="profile-index__feed-item">
              <img src={picture} alt="" className="profile-index__feed-image" />
              <div>
                <p className="profile-index__feed-name">
                  Dima.vasiliev.98
                  <span className="profile-index__feed-date">
                    2 месяца назад
                  </span>
                </p>
                <p className="profile-index__item-desc">
                  Выполнил(а) тренировку{" "}
                  <a href="" className="profile-index__feed-link">
                    "Пресс 2/30"
                  </a>{" "}
                  по программе{" "}
                  <a href="" className="profile-index__feed-link">
                    "Стальной пресс"
                  </a>
                </p>
              </div>
            </div>
            <div className="profile-index__feed-item">
              <img src={picture} alt="" className="profile-index__feed-image" />
              <div>
                <p className="profile-index__feed-name">
                  Dima.vasiliev.98
                  <span className="profile-index__feed-date">
                    2 месяца назад
                  </span>
                </p>
                <p className="profile-index__item-desc">
                  Выполнил(а) тренировку{" "}
                  <a href="" className="profile-index__feed-link">
                    "Пресс 2/30"
                  </a>{" "}
                  по программе{" "}
                  <a href="" className="profile-index__feed-link">
                    "Стальной пресс"
                  </a>
                </p>
              </div>
            </div>
            <div className="profile-index__feed-item">
              <img src={picture} alt="" className="profile-index__feed-image" />
              <div>
                <p className="profile-index__feed-name">
                  Dima.vasiliev.98
                  <span className="profile-index__feed-date">
                    2 месяца назад
                  </span>
                </p>
                <p className="profile-index__item-desc">
                  Выполнил(а) тренировку{" "}
                  <a href="" className="profile-index__feed-link">
                    "Пресс 2/30"
                  </a>{" "}
                  по программе{" "}
                  <a href="" className="profile-index__feed-link">
                    "Стальной пресс"
                  </a>
                </p>
              </div>
            </div>
            <div className="profile-index__feed-item">
              <img src={picture} alt="" className="profile-index__feed-image" />
              <div>
                <p className="profile-index__feed-name">
                  Dima.vasiliev.98
                  <span className="profile-index__feed-date">
                    2 месяца назад
                  </span>
                </p>
                <p className="profile-index__item-desc">
                  Выполнил(а) тренировку{" "}
                  <a href="" className="profile-index__feed-link">
                    "Пресс 2/30"
                  </a>{" "}
                  по программе{" "}
                  <a href="" className="profile-index__feed-link">
                    "Стальной пресс"
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="profile-index__achieved">
            <h3 className="prof">Мои достижения</h3>
            <div className="profile-index__achieved-container">
            <div className="profile-index__achieved-item">
                <img src={daysIcon} alt="" className="profile-index__achieved-image" />
                <div className="profile-index__achieved-box">
                    <div className="profile-index__achieved-num">0 дней</div>
                    <div className="profile-index__achieved-desc">На платформе</div>
                </div>
            </div>
            <div className="profile-index__achieved-item">
                <img src={daysIcon} alt="" className="profile-index__achieved-image" />
                <div className="profile-index__achieved-box">
                    <div className="profile-index__achieved-num">0 часов</div>
                    <div className="profile-index__achieved-desc">Энергии потрачено</div>
                </div>
            </div>
            <div className="profile-index__achieved-item">
                <img src={daysIcon} alt="" className="profile-index__achieved-image" />
                <div className="profile-index__achieved-box">
                    <div className="profile-index__achieved-num">0 занятий</div>
                    <div className="profile-index__achieved-desc">Выполнено</div>
                </div>
            </div>
            <div className="profile-index__achieved-item">
                <img src={daysIcon} alt="" className="profile-index__achieved-image" />
                <div className="profile-index__achieved-box">
                    <div className="profile-index__achieved-num">0 Ккал</div>
                    <div className="profile-index__achieved-desc">Сожжено калорий</div>
                </div>
            </div>
            </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileIndex;
