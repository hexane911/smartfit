import { useState } from "react";
import Button from "../../components/button";
import TextField from "../../components/text-field";
import cameraIcon from '../../assets/images/camera-plus.svg'
import examplePhoto from '../../assets/images/example-photo.png'
import "./progress.sass";

const Progress = () => {
  const [state, setState] = useState<0 | 1 | 2>(0);
  return (
    <div className="progress">
      <div className="progress__layout">
        <div className="progress__fields">
          <h3 className="prof progress__title-nm">Параметры</h3>
          <TextField
            button={{ text: "Сохранить", onClick: () => {} }}
            label="Вес"
            placeholder="Вес (кг)"
          />
          <TextField
            button={{ text: "Сохранить", onClick: () => {} }}
            label="Обхват талии"
            placeholder="Обхват талии (см)"
          />
          <TextField
            button={{ text: "Сохранить", onClick: () => {} }}
            label="Обхват бёдер"
            placeholder="Обхват бёдер (см)"
          />
          <TextField
            button={{ text: "Сохранить", onClick: () => {} }}
            label="Обхват груди"
            placeholder="Обхват груди (см)"
          />
          <TextField
            button={{ text: "Сохранить", onClick: () => {} }}
            label="Обхват рук"
            placeholder="Обхват рук (см)"
          />
          <TextField
            button={{ text: "Сохранить", onClick: () => {} }}
            label="Обхват ног"
            placeholder="Обхват ног (см)"
          />
        </div>
        <div className="progress__photos">
          <h3 className="prof">Фотографии</h3>
          <p className="progress__subtitle">Формат</p>
          <div className="progress__switcher">
            <Button
              classname="progress__button"
              clear={!(state === 0)}
              onClick={() => {
                setState(0);
              }}
            >
              Спереди
            </Button>
            <Button
              classname="progress__button"
              clear={!(state === 1)}
              onClick={() => {
                setState(1);
              }}
            >
              Сбоку
            </Button>
            <Button
              classname="progress__button"
              clear={!(state === 2)}
              onClick={() => {
                setState(2);
              }}
            >
              Сзади
            </Button>
          </div>
          <div className="progress__content">
            <div className="progress__item">
              <p className="progress__subtitle">Первое фото</p>
              <div className="progress__frame" style={{
                backgroundImage: examplePhoto && `url(${examplePhoto})` //вот такую конструкцию вернём, если фото есть
                
              }}>
                <div className="progress__date">14.09.2022</div>
              </div>
            </div>
            <div className="progress__item">
              <p className="progress__subtitle">Последнее фото</p>
              <div className="progress__add">
                <input type="file" className="progress__add-input" />
                <img src={cameraIcon} alt="" className="progress__add-icon" />
              </div>
                
            </div>
          </div>
          <Button classname="progress__button bottom" onClick={()=>{}}>загрузить фото</Button>
        </div>
      </div>
    </div>
  );
};

export default Progress;
