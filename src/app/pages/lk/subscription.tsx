import Button from "../../components/button"
import visaLogo from '../../assets/images/methods/visa.svg'
import deleteIcon from '../../assets/images/methods/delete.svg'
import okIcon from '../../assets/images/methods/ok.svg'
import './subscription.sass'

const samplePurchases : Purchase[] = [
 {
    id: 292861,
    purchaseDate: '26.05.2022 г.',
    status: 'Оплачено',
    subscriptionType: 'Безлимит (подписка)',
    summ: 3200,
    promo: 'SMART'
 },
 {
    id: 292861,
    purchaseDate: '26.05.2022 г.',
    status: 'Оплачено',
    subscriptionType: 'Безлимит (подписка)',
    summ: 3200,
    promo: 'SMART'
 },
 {
    id: 292861,
    purchaseDate: '26.05.2022 г.',
    status: 'Оплачено',
    subscriptionType: 'Безлимит (подписка)',
    summ: 3200,
    promo: 'SMART'
 },
 {
    id: 292861,
    purchaseDate: '26.05.2022 г.',
    status: 'Оплачено',
    subscriptionType: 'Безлимит (подписка)',
    summ: 3200,
    promo: 'SMART'
 },
 {
    id: 292861,
    purchaseDate: '26.05.2022 г.',
    status: 'Оплачено',
    subscriptionType: 'Безлимит (подписка)',
    summ: 3200,
    promo: 'SMART'
 },

]

interface Purchase {
    id: number,
    subscriptionType: string,
    status: string,
    purchaseDate: string //заменить на Date
    promo?: string
    summ: number 
}

const Subscription = ()=> {
    const subActive = false
    return <div className="subscription">
        <div className="subscription__top">
            <h3 className="subscription__title">Моя подписка</h3>
            <Button classname={`subscription__button`} red={!subActive} onClick={()=>{}}>{subActive ? 'активна' :  "не активна"}</Button>
        </div>
        <h4 className="subscription__subtitle">Способы оплаты</h4>
        <div className="subscription__methods">
            <div className="subscription__method">
                <img src={visaLogo} alt="" className="subscription__method-icon" />
                <p className="subscription__card">
                427655******5215
                </p>
                <p className="subscription__method-date">
                    <span>Дата привязки:</span>
                    27.03.2022
                </p>
                <div className="subscription__delete">
                    <img src={deleteIcon} alt="" />
                    Удалить
                </div>
            </div>
        </div>
        <h4 className="subscription__subtitle">История заказов</h4>
        <div className="subscription__table">
            <div className="subscription__headers grid">
                <div className="subscription__header">№ заказа</div>
                <div className="subscription__header">Подписка</div>
                <div className="subscription__header">Статус</div>
                <div className="subscription__header">Дата списания</div>
                <div className="subscription__header">Промокод</div>
                <div className="subscription__header">Сумма</div>
            </div>
            {samplePurchases.map(el => {
                return  (
                    <div className="subscription__item grid">
                        <div className="subscription__item-field">{el.id}</div>
                        <div className="subscription__item-field">{el.subscriptionType}</div>
                        <div className="subscription__item-field">{el.status === "Оплачено" && (
                            <>
                            <img src={okIcon} alt="" className="subscription__item-icon" />
                            {el.status}
                            </>
                        )}</div>
                        <div className="subscription__item-field">{el.purchaseDate}</div>
                        <div className="subscription__item-field">{el.promo}</div>
                        <div className="subscription__item-field">{el.summ} р</div>
                    </div>
                )
            })}
        </div>

    </div>
}

export default Subscription