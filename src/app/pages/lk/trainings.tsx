import { useEffect, useRef, useState } from "react";
import "./trainings.sass";
import sampleVideo from "../../assets/images/sample-video.png";
import rewindIcon from "../../assets/images/rewind.svg";
import fireIcon from "../../assets/images/fire.svg";
import playIcon from "../../assets/images/play-circle.svg";
import heartIcon from '../../assets/images/heart.svg'
import sampleProgram from '../../assets/images/sample-program-small.png'
import Comment from "../../components/comment";
import TextArea from "../../components/text-area";
import SubsModal from "../../components/modals/subs.moda";


const Trainings = () => {
  const scrollRef = useRef<HTMLInputElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);
  const [value, setValue] = useState(0);
  useEffect(() => {
    if (scrollRef.current && containerRef.current) {
      const total =
        containerRef.current.scrollWidth - containerRef.current.offsetWidth;
      containerRef.current.scrollLeft = total * (value / 100);
    }
  }, [value]);

  return (
    <div className="trainings">
      <div className="trainings__top">
        <p className="trainings__name">Стальной пресс</p>
        <p className="trainings__num">1/30</p>
      </div>
      <div className="trainings__video">
        <img src={sampleVideo} alt="" className="trainings__sample" />
        <img src={playIcon} alt="" className="trainings__play" />
      </div>
      <input
        type="range"
        className="trainings__input"
        defaultValue={0}
        onChange={(e) => {
          setValue(+e.target.value);
        }}
        id={"scrolling__bar"}
        ref={scrollRef}
      />
      <div className="trainings__wrapper" ref={containerRef}>
        <div className="trainings__trainings">
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
          <TrainingItem />
        </div>
      </div>
      <div className="trainings__comments">
        <div className="trainings__comments-top">
            <img src={sampleProgram} alt="" className="trainings__program-image" />
            <div>
                <p className="trainings__program-name">Дмитрий Васильев</p>
                <p className="trainings__program-title">Стальной пресс</p>
            </div>
            <div className="last">
                <p className="trainings__program-likes">1235 <img src={heartIcon} alt="" className="trainings__program-like" /></p>
                <p className="trainings__program-completed">123132123 выполнений</p>
            </div>
        </div>
        <Comment />
        <Comment />
        <Comment />
        <Comment />
      </div>
      <div className="trainings__review">
        <TextArea label="Ваш отзыв" placeholder="Текст отзыва" className=""></TextArea>
      </div>
    </div>
  );
};

const TrainingItem = () => {
  return (
    <div className="trainings__item">
      <p className="trainings__item-num">1/30</p>
      <div
        className="trainings__item-frame"
        style={{
          backgroundImage: `url(${sampleVideo})`,
        }}
      >
        <p className="trainings__item-time">29.18</p>
        <img src={rewindIcon} alt="" className="trainings__item-icon" />
      </div>
      <p className="trainings__item-burn">
        <img src={fireIcon} alt="" className="trainings__item-fire" />
        520 ккал
      </p>
    </div>
  );
};

export default Trainings;
