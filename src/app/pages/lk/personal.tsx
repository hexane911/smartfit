import Button from "../../components/button";
import "./personal.sass";
import placeholder from "../../assets/images/Avatar.svg";
import TextField from "../../components/text-field";
import { useState } from "react";
import PasswordModal from "../../components/modals/password.modal";
import { useWindowSize } from "../../hooks";

const Personal = () => {
  const [level, setLevel] = useState<0 | 1 | 2>(0);
  const [modalOpen, setModalOpen] = useState(false);
  const { width } = useWindowSize();
  return (
    <div className="personal">
      <PasswordModal
        isOpen={modalOpen}
        action={() => setModalOpen(false)}
      ></PasswordModal>
      <h3 className="prof">Личные данные</h3>
      <div className="personal__avatar">
        <img src={placeholder} alt="" className="personal__avatar-image" />
        <div className="personal__avatar-box">
          <div className="personal__avatar-buttons">
            <Button
              onClick={() => {}}
              clear
              classname="personal__button-avatar"
            >
              Сменить фото
            </Button>
            <Button onClick={() => {}} classname="personal__button-avatar">
              Удалить
            </Button>
          </div>
          {width > 419 && (
            <p className="personal__avatar-misc">
              Допустимые типы файлов JPEG, <br /> GIF or PNG, 150x150px, 1MB
            </p>
          )}
        </div>
      </div>
      {width <= 419 && (
        <p className="personal__avatar-misc">
          Допустимые типы файлов JPEG, <br /> GIF or PNG, 150x150px, 1MB
        </p>
      )}
      {width > 419 && (
        <div className="personal__layout">
          <TextField
            className="personal__input"
            label="Ваше имя"
            placeholder="Имя"
          />
          <div className="personal__buttons-box">
            Уровень активности
            <div className="personal__buttons">
              <Button
                classname="personal__button"
                clear={!(level === 0)}
                onClick={() => {
                  setLevel(0);
                }}
              >
                начальный
              </Button>
              <Button
                classname="personal__button"
                clear={!(level === 1)}
                onClick={() => {
                  setLevel(1);
                }}
              >
                средний
              </Button>
              <Button
                classname="personal__button"
                clear={!(level === 2)}
                onClick={() => {
                  setLevel(2);
                }}
              >
                эксперт
              </Button>
            </div>
          </div>
          <TextField
            className="personal__input"
            label="Ваша фамилия"
            placeholder="Фамилия"
          />
          <TextField
            className="personal__input"
            label="Ваш вес"
            placeholder="Вес"
          />
          <TextField
            className="personal__input"
            label="Ваш телефон"
            placeholder="Телефон"
          />
          <TextField
            className="personal__input"
            label="Ваш рост"
            placeholder="Рост (см)"
          />
          <TextField
            className="personal__input"
            label="Ваш Email"
            placeholder="Адрес электронной почты"
          />
          <TextField
            className="personal__input"
            label="Ваш пол"
            placeholder="Пол"
          />
          <TextField
            className="personal__input"
            label="Ваш Instagram"
            placeholder="/instagram.com"
          />
          <TextField
            className="personal__input"
            label="Ваша дата рождения"
            placeholder="Адрес электронной почты"
          />
          <div className="spacer"></div>
          <div className="personal__bottom">
            <Button
              classname="personal__button"
              clear
              onClick={() => setModalOpen(true)}
            >
              Сменить пароль
            </Button>
            <Button classname="personal__button" onClick={() => {}}>
              Изменить данные
            </Button>
          </div>
        </div>
      )}
      {width <= 419 && (
        <div className="personal__layout">
          <TextField
            className="personal__input"
            label="Ваше имя"
            placeholder="Имя"
          />
          <TextField
            className="personal__input"
            label="Ваша фамилия"
            placeholder="Фамилия"
          />

          <TextField
            className="personal__input"
            label="Ваш телефон"
            placeholder="Телефон"
          />
          <TextField
            className="personal__input"
            label="Ваш Email"
            placeholder="Адрес электронной почты"
          />
          <TextField
            className="personal__input"
            label="Ваш Instagram"
            placeholder="/instagram.com"
          />
          <div className="personal__buttons-box">
            Уровень активности
            <div className="personal__buttons">
              <Button
                classname="personal__button"
                clear={!(level === 0)}
                onClick={() => {
                  setLevel(0);
                }}
              >
                начальный
              </Button>
              <Button
                classname="personal__button"
                clear={!(level === 1)}
                onClick={() => {
                  setLevel(1);
                }}
              >
                средний
              </Button>
              <Button
                classname="personal__button"
                clear={!(level === 2)}
                onClick={() => {
                  setLevel(2);
                }}
              >
                эксперт
              </Button>
            </div>
          </div>
          <TextField
            className="personal__input"
            label="Ваш вес"
            placeholder="Вес"
          />
          <TextField
            className="personal__input"
            label="Ваш рост"
            placeholder="Рост (см)"
          />
          
          
          <TextField
            className="personal__input"
            label="Ваш пол"
            placeholder="Пол"
          />
          
          <TextField
            className="personal__input"
            label="Ваша дата рождения"
            placeholder="Адрес электронной почты"
          />
          <div className="spacer"></div>
          <div className="personal__bottom">
            <Button
              classname="personal__button"
              clear
              onClick={() => setModalOpen(true)}
            >
              Сменить пароль
            </Button>
            <Button classname="personal__button" onClick={() => {}}>
              Изменить данные
            </Button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Personal;
