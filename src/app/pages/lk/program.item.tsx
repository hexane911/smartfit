import ScrolligContent from "../../components/scrolling-content"
import { Program } from "../../types/program.type"
import './program.item.sass'
import dots from '../../assets/images/dots.svg'

const ProgramItem = ({image, name, title, desc} : Program) => {
    return (
        <div className="program-item">
            <div className="program-item__top">
                <img src={image} alt="" className="program-item__image" />
                <div className="program-item__info">
                    <img src={dots} alt="" className="program-item__dots" />
                    <p className="program-item__title">{title}</p>
                    <p className="program-item__name">{name}</p>
                    <p className="program-item__desc">{desc}</p>
                </div>
            </div>
            <ScrolligContent small className="program-item__scroll"/>
        </div>
    )
}

export default ProgramItem