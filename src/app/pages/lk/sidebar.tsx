import { Link, useLocation, useParams } from "react-router-dom";

import profileIcon from "../../assets/images/lk/profile.svg";
import profileIconGreen from "../../assets/images/lk/profile-1.svg";
import dataIcon from "../../assets/images/lk/data.svg";
import dataIconGreen from "../../assets/images/lk/data-1.svg";
import programsIcon from "../../assets/images/lk/programs.svg";
import programsIconGreen from "../../assets/images/lk/programs-1.svg";
import trainingsIcon from "../../assets/images/lk/trainings.svg";
import trainingsIconGreen from "../../assets/images/lk/trainings-1.svg";
import progressIcon from "../../assets/images/lk/progress.svg";
import progressIconGreen from "../../assets/images/lk/progress-1.svg";
import subsIcon from "../../assets/images/lk/subs.svg";
import subsIconGreen from "../../assets/images/lk/subsc-1.svg";
import logoutIcon from '../../assets/images/lk/logout-1.svg'

import "./sidebar.sass";
import { useState } from "react";
import LogoutModal from "../../components/modals/logout.modal";

const Sidebar = () => {
  const pathname = useLocation().pathname;
  const [modalOpen, setModalOpen] = useState(false)

  return (
    <div className="sidebar">
        <LogoutModal isOpen={modalOpen} action={() => setModalOpen(false)}/>

      <div className="sidebar__list">
        <Link to="/profile" className={`sidebar__item ${pathname === "/profile" ? "active" : ''}`}>
          <img
            src={pathname === "/profile" ? profileIconGreen : profileIcon}
            alt=""
            className="sidebar__icon"
          />
          Профиль
        </Link>
        <Link to="personal" className={`sidebar__item ${pathname.includes("personal") ? "active" : ''}`}>
          <img
            src={pathname.includes("personal") ? dataIconGreen : dataIcon}
            alt=""
            className="sidebar__icon"
          />
          Данные
        </Link>
        <Link to="programs" className={`sidebar__item ${pathname.includes("programs") ? "active" : ''}`}>
          <img
            src={pathname.includes("programs") ? programsIconGreen : programsIcon}
            alt=""
            className="sidebar__icon"
          />
          Программы
        </Link>
        <Link to="trainings" className={`sidebar__item ${pathname.includes("trainings") ? "active" : ''}`}>
          <img
            src={pathname.includes("trainings") ? trainingsIconGreen : trainingsIcon}
            alt=""
            className="sidebar__icon"
          />
          Тренировки
        </Link>
        <Link to="progress" className={`sidebar__item ${pathname.includes("progress") ? "active" : ''}`}>
          <img
            src={pathname.includes("progress") ? progressIconGreen : progressIcon}
            alt=""
            className="sidebar__icon"
          />
          Прогресс
        </Link>
        <Link to="subscription" className={`sidebar__item ${pathname.includes("subscription") ? "active" : ''}`}>
          <img
            src={pathname.includes("subscription") ? subsIconGreen : subsIcon}
            alt=""
            className="sidebar__icon"
          />
          Подписка
        </Link>
      <div className="sidebar__line" />
      <div className="sidebar__logout" onClick={() => setModalOpen(true)}>
        <img src={logoutIcon} alt="" className="sidebar__icon" />
        Выход
      </div>
      </div>
    </div>
  );
};

export default Sidebar;
