import { useState } from "react";
import Button from "../components/button";
import BuyModal from "../components/modals/buy.modal";
import { SAMPLE_QUESTIONS, SAMPLE_REVIEWS } from "../temp/data";
import "./purchase.page.sass";
import Questions from "./questions.section";
import Reviews from "./reviews.section";
import VideoSection from "./video.section";

interface Buy {
  title: string,
  price: number,
  oldPrice: number
}

const PurchasePage = () => {
  const [modalOpen, setModalOpen] = useState(false)
  const [data, setData] = useState<Buy | null>(null)
  const handleOpen = (data : Buy) => {
    if(data) {
      setData(data)
      setModalOpen(true)
    }
  }
  return (
    <div className="purchase page">
      {data && <BuyModal isOpen={modalOpen} action={() => setModalOpen(false)} {...data}></BuyModal>}
      <div className="purchase__inner wrapper">
        <h2 className="purchase__title">
          Начни тренироваться сейчас, <br /> сделай вклад в своё здоровье
        </h2>
        <p className="purchase__subtitle _18 center">
          Сбалансированное питание, тренировки, помощь куратора <br /> нашей
          платформы, поддержка других участников. <br /><b>Итог: ваша цель достигнута</b>
        </p>
        <div className="purchase__box">
            <div className="purchase__item">
                <p className="purchase__item-top _18">Тариф</p>
                <p className="purchase__item-title">3 МЕСЯЦА (ПОДПИСКА)</p>
                <ul className="purchase__item-list">
                    <li className="purchase__item-li">Lorem ipsum dolor sit, amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                </ul>
                <div className="purchase__item-box">
                    <div className="purchase__item-price">2490 р.</div>
                    <div className="purchase__item-price purchase__item-price-old">9490 р.</div>
                </div>
                <Button classname="purchase__button" onClick={()=>{
                  handleOpen({
                    oldPrice: 9490,
                    price: 2490,
                    title: '3 месяца (подписка)'
                  })
                }}>тренироваться</Button>
            </div>
            <div className="purchase__item">
                <p className="purchase__item-top _18">Тариф</p>
                <p className="purchase__item-title">12 МЕСЯЦЕВ (ПОДПИСКА)</p>
                <ul className="purchase__item-list">
                    <li className="purchase__item-li">Lorem ipsum dolor sit, amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                </ul>
                <div className="purchase__item-box">
                    <div className="purchase__item-price">2490 р.</div>
                    <div className="purchase__item-price purchase__item-price-old">9490 р.</div>
                </div>
                <Button classname="purchase__button" onClick={()=>{
                  handleOpen({
                    oldPrice: 9490,
                    price: 2490,
                    title: '12 месяцев (подписка)'
                  })
                }}>тренироваться</Button>
            </div>
            <div className="purchase__item">
                <p className="purchase__item-top _18">Тариф</p>
                <p className="purchase__item-title">БЕЗЛИМИТ НАВСЕГДА</p>
                <ul className="purchase__item-list">
                    <li className="purchase__item-li">Lorem ipsum dolor sit, amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                    <li className="purchase__item-li">Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                </ul>
                <div className="purchase__item-box">
                    <div className="purchase__item-price">2490 р.</div>
                    <div className="purchase__item-price purchase__item-price-old">9490 р.</div>
                </div>
                <Button classname="purchase__button" onClick={()=>{
                  handleOpen({
                    oldPrice: 9490,
                    price: 2490,
                    title: 'Безлимит навсегда'
                  })
                }}>тренироваться</Button>
            </div>
        </div>
      <VideoSection/>
      </div>
      <Reviews props={SAMPLE_REVIEWS} />
      <Questions props={SAMPLE_QUESTIONS} />
    </div>
  );
};

export default PurchasePage;
