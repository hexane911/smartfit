import { MouseEventHandler, useState } from 'react'
import { Question } from '../types/question'
import crossIcon from '../assets/images/cross.svg'
import './questions.section.sass'
import { SAMPLE_QUESTIONS } from '../temp/data'





type Props = {
    props : Question[]
}

const Questions = ({props} : Props) => {
    const initial = Array.apply(null, Array(props.length)).map((el , i) => {
        return i===0? true : false
    })
    const closed = Array.apply(null, Array(props.length)).map((el , i) => {
        return false
    })

    const [state, setState] = useState(initial)

    const handleChange = (index : number) => {
        let result = closed
        if (!state[index]) {
            result[index] = true
        }
        setState(result)
    }
    
    return <div className="questions">
        <div className="wrapper questions__inner">
            <h2 className="questions__title">
            Остались <span className="underline small">вопросы</span>?
            </h2>
            <p className="questions__subtitle _18 center">Мы собрали самые часто задаваемые вопросы для вас</p>
            <div className="questions__box">
                {SAMPLE_QUESTIONS.map((el, i) => {
                    return <Questionitem handler={handleChange} item={el} index={i} state={state[i]} />
                })}
            </div>
        </div>
    </div>
}

type ItemProps = {
    item : Question,
    index: number,
    state: boolean,
    handler : any
}

const Questionitem = ({item, index, state, handler} : ItemProps) => {

    return <div className="questions__item" onClick={() => {handler(index)}}>
        <div className="questions__item-top">
            <p className="questions__item-title">{item.title}</p>
            <img src={crossIcon} alt="" className={`questions__item-button ${state ? '_opened' : ''}`} />
        </div>
        <div className={`questions__item-body ${state ? '_opened' : ''}`}>
            {state && item.text}
        </div>
    </div>
}

export default Questions