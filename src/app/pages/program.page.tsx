import './program.page.sass'
import './trainer.page.sass'
import outline from "../assets/images/outline.svg";
import example from "../assets/images/dmitri.png";
import Button from '../components/button';
import Carousel from '../components/carousel';
import { SAMPLE_PROGRAMS, SAMPLE_QUESTIONS, SAMPLE_REVIEWS } from '../temp/data';
import ProgramSlide from '../components/slides/program.slide';
import Reviews from './reviews.section';
import Questions from './questions.section';

import videoicon from '../assets/images/video.svg'
import ScrolligContent from '../components/scrolling-content';

const ProgramPage = () => {
    return <div className="program-page trainer">
        <div className="trainer__upper">
        <div className="trainer__inner h wrapper">
          <div className="trainer__left">
            <p className="trainer__subtitle">Программа</p>
            <h1 className="trainer__title">СТАЛЬНОЙ ПРЕСС</h1>
            <p className="trainer__info">
              <span>12</span> Тренировок
в программе
            </p>
            <p className="trainer__info">
              <span>150</span>Минут длится одна тренировка
            </p>
            <p className="trainer__info">
              <span>15671</span>Калорий сжигается за тренировку 
            </p>
            <Button icon='bolt' classname="trainer__button" onClick={() => {}}>
              начать тренировку
            </Button>
          </div>
          <div className="trainer__frame">
            <img src={outline} alt="" className="trainer__background" />
            <img src={example} alt="" className="trainer__image" />
          </div>
          <Button icon='bolt' classname="trainer__button mobile" onClick={() => {}}>
              начать тренировку
          </Button>
        </div>
      </div>
      <div className="trainer__inner wrapper">
        <div className="video program__video">
          <div className="video__left program__frame">
            <img src={videoicon} alt="" className="program__play" />
          </div>
          <div className="video__right">
            <h2 className="video__title program__title">О программе</h2>
            <p className="_18 video__desc">
            Тебя ждет 30 тренировок и 6 уровней сложности – с каждым уровнем нарастает сложность и интенсивность тренировок! Ты будешь наглядно видеть свой прогресс!
Начни новую жизнь уже сегодня, с улыбкой на лице – вместе с Дмитрием Васильевым.
            </p>
            <div className="program__items">
              <div className="program__item"><span>30</span>
              Тренировок <br />
в программе
              </div>
              <div className="program__item"><span>30</span>
              Минут длится <br /> одна тренировка
              </div>
              <div className="program__item"><span>30</span>
              Калорий сжигается <br /> за тренировку 
              </div>
            </div>
          </div>
        </div>
        <ScrolligContent className='program__scrolling' />
        <h2 className="center trainer__h2 m">О <span className="underline">тренере</span></h2>
        <div className="trainer__about video">
          <div className="video__left">
            <Button icon='video' onClick={() => {}} classname="video__button">
              видео о тренере
            </Button>
          </div>
          <div className="video__right">
            <p className="_18 video__desc">
              Вы давно мечтаете похудеть, нарастить мышечную массу или
              сформировать красивый рельеф? С платформой SmartFit вам откроются
              новые возможности онлайн-фитнеса с гарантированным результатом!
              <br /><br />
              Наша команда тренеров с многолетним опытом подготовила программы
              видеотренировок для различных целей: похудение, набор массы,
              рельеф, растяжка и многое другое.
              <br /><br />
               Ваш результат начинается со
              SmartFit! Создайте тело мечты!
            </p>
          </div>
        </div>
        <h2 className="trainer__h2 center">
          Программы с <span className="underline small">тренером</span>
        </h2>
        <p className="_18 trainer__p center">
          Занимайтесь под руководством профессионалов
        </p>
        <Carousel className="trainer__others" noArrows>
          {SAMPLE_PROGRAMS.map((el) => (
            <ProgramSlide {...el} />
          ))}
        </Carousel>
      </div>
      <Reviews props={SAMPLE_REVIEWS} />
      <Questions props={SAMPLE_QUESTIONS} />
    </div>
}

export default ProgramPage