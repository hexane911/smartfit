import Carousel from '../components/carousel'
import ReviewSlide from '../components/slides/review.slide'
import { SAMPLE_REVIEWS } from '../temp/data'
import { Review } from '../types/review'
import './reviews.section.sass'

type Props = {
 props: Review[]
}

const Reviews = ({props} : Props) => {
    return <div className="reviews">
        <div className="reviews__inner wrapper">
            <h2 className="reviews__title center">
                <span className="underline small">Отзывы</span>
            </h2>
            <p className="_18 reviews__subtitle center">Вот что о нас говорят подопечные</p>
            <div className="reviews__box">
                <Carousel className='reviews' arrowsDown >
                    {
                        SAMPLE_REVIEWS.map(el => {
                            return <ReviewSlide {...el}/>
                        })
                    }
                </Carousel>
            </div>
        </div>
    </div>
}

export default Reviews