import Button from "../components/button";
import "./video.section.sass";


const VideoSection = () => {
  return (
    <div className="video">
      <div className="video__left">
        <Button icon="video" onClick={() => {}} classname="video__button" >видео о проекте</Button>
      </div>
      <div className="video__right">
        <h2 className="video__title left">
          С чего начинается ваш{" "}
          <span className="underline small">результат</span>?
        </h2>
        <p className="_18 video__desc">
          Вы давно мечтаете похудеть, нарастить <br /> мышечную массу или сформировать
          красивый рельеф? С платформой SmartFit вам откроются новые возможности
          онлайн-фитнеса с гарантированным результатом! <br /> <br />
          Наша команда тренеров с многолетним опытом подготовила программы
          видеотренировок для различных целей: похудение, набор массы, рельеф,
          растяжка и многое другое. <br /> <br />
          Ваш результат начинается со SmartFit! Создайте тело мечты!
        </p>
      </div>
    </div>
  );
};

export default VideoSection;
