import { useState } from 'react'
import Button from '../components/button'
import BlogSlide from '../components/slides/blog.slide'
import { SAMPLE_BLOG } from '../temp/data'
import './blog.page.sass'

const BlogPage = () => {
    const defaultNum = 9
    const [left, setLeft] = useState(SAMPLE_BLOG.length - defaultNum)
    const [limit, setLimit] = useState(defaultNum)

    const handleLimit = () => {
        if (left) {
            setLeft(left => left - defaultNum)
            setLimit(l => l + defaultNum)
        }
    }
    return <div className="blog page">
        <div className="blog__inner wrapper">
            <h2 className="blog__title center">
                Прокачайте свои знания на нашем блоге
            </h2>
            <p className="_18 blog__subtitle center">Начните лучше понимать своё тело</p>
            <div className="blog__box">
                {SAMPLE_BLOG.map((el, i) => {
                    return i < limit ? <BlogSlide className='blog__slide' {...el} /> : null
                })}
            </div>
            {left > 0 && <Button  classname='blog__button' onClick={handleLimit}>Показать ещё</Button>}
        </div>
    </div>
}

export default BlogPage