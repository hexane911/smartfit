import "./post.page.sass";
import example from "../assets/images/blog-example.png";
import TextField from "../components/text-field";
import TextArea from "../components/text-area";
import Comment from "../components/comment";
import Carousel from "../components/carousel";
import { SAMPLE_BLOG } from "../temp/data";
import BlogSlide from "../components/slides/blog.slide";

const PostPage = () => {
  return (
    <div className="post-page page">
      <div className="post-page__wrapper wrapper">
        <h1 className="post-page__title title">
          Топ-5 рецептов для снижения веса в домашних условиях
        </h1>
        <div className="post-page__info">
          {
            "22.09.2023" // дата
          }
          <div className="post-page__dot"></div>
          {
            "Время прочтения 5 минут" // время прочтения
          }
        </div>
        <div className="post-page__content">
          <img src={example} alt="" className="post-page__image" />
          <p className="post-page__paragraph">
            Любишь завтракать на ходу или вовсе обходишься чашечкой кофе с утра?
            А ведь именно завтрак задает настрой на весь день. <br />
            <br />
            Посвяти 15 минут вкусному полезному завтраку и ты почувствуешь
            прилив сил и бодрости, а ранний подъем не будет казаться таким уж и
            ранним. <br />
            <br />
            Мы подобрали для тебя 5 самых полезных рецептов на завтрак, которые
            насытят организм энергией и удержат тебя от частых перекусов.
          </p>
          <h2 className="post-page__subtitle">Ленивая овсянка в банке</h2>
          <p className="post-page__paragraph">
            Любишь завтракать на ходу или вовсе обходишься чашечкой кофе с утра?
            А ведь именно завтрак задает настрой на весь день. <br />
            <br />
            Посвяти 15 минут вкусному полезному завтраку и ты почувствуешь
            прилив сил и бодрости, а ранний подъем не будет казаться таким уж и
            ранним. <br />
            <br />
            Мы подобрали для тебя 5 самых полезных рецептов на завтрак, которые
            насытят организм энергией и удержат тебя от частых перекусов.
          </p>
        </div>
        <div className="post-page__form">
          <h1 className="post-page__title title form-title">Отзывы о статье</h1>
          <div className="post-page__form-top">
            <TextField label="Ваше имя" placeholder="Имя" />
            <TextField label="Ваша почта" placeholder="Почта" />
          </div>
          <TextArea label="Ваш отзыв" className="" placeholder="Текст отзыва" />
        </div>
        <div className="post-page__comments">
          <Comment />
          <Comment />
          <Comment />
          <Comment />
          <Comment />
        </div>

        <div className="post-page__similar">
          <h2 className="post-page__sim-title">
            Похожие <span className="underline">статьи</span>
          </h2>
          <p className="post-page__sim-sub">
            Выберите то что подходит именно вам!
          </p>
          <Carousel className="post-page__slider">
            {SAMPLE_BLOG.map((el, i) => {
              return i > 5 ? null : (
                <BlogSlide className="post-page__slide" {...el} />
              );
            })}
          </Carousel>
        </div>
      </div>
    </div>
  );
};

export default PostPage;
