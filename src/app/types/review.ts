export type Review = {
    name: string;
    text: string;
    image: string;
}