export type Program = {
    image: string,
    videos : number,
    time: string,
    calories: number,
    name: string,
    title?: string,
    desc?: string

}

export type ProgramVideo = {
    preview: string,
    length: string,


}