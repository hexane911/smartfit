export type User = {
    name: string,
    surname: string,
    nickname: string,
    phone: string,
    email: string,
    instagram?: string,
    weight? : number,
    height?: number,
    gender: 'male' | 'female',
    birthday?: Date
    

}