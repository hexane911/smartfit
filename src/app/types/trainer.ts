export type Trainer = {
    name: string,
    image: string,
    videos: number,
    programs: number,
    people: number
}