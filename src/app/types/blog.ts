export type Blog = {
    image: string,
    title: string,
    likes: number,
    date: string
}