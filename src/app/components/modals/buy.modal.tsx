import Button from "../button"
import AppModal from "../modal"
import TextField from "../text-field"
import './buy.modal.sass'
import { IModal } from "./modal.interface"

interface Buy extends IModal {
    title: string,
    price: number,
    oldPrice: number
}

const BuyModal = ({isOpen, oldPrice, price,action, title} : Buy) => {
    return <AppModal isOpen={isOpen} closeButton={
        {
            show: true,
            onClick: action
        }
    }>
        <div className="buy-modal">
            <h2 className="modal-title buy-modal__title">{title}</h2>
            <div className="buy-modal__box">
                <TextField label="Ваша почта" placeholder="Адрес электронной почты"></TextField>
                <TextField label="Ваш телефон" placeholder="Номер телефона"></TextField>
                <TextField label="Промокод" button={{
                    text: 'ok',
                    onClick: ()=>{}
                }} placeholder="Промокод"></TextField>
            </div>
            <div className="buy-modal__pricebox">
                Стоимость:
                <p className="buy-modal__old">{oldPrice} р</p>
                <p className="buy-modal__price">{price} р</p>
            </div>
            <Button classname="buy-modal__button" onClick={()=>{}}>оплатить</Button>
        </div>
    </AppModal>
}

export default BuyModal