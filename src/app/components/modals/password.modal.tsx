import Button from "../button"
import AppModal from "../modal"
import TextField from "../text-field"
import { IModal } from "./modal.interface"

import './password.modal.sass'

const PasswordModal = ({isOpen, action} : IModal) =>{
    return <AppModal isOpen={isOpen} closeButton={{
        show: true,
        onClick: action
    }}>
        <div className="password-modal">
            <h2 className="modal-title password-modal__title">Смена пароля</h2>
            <div className="password-modal__box">
                <TextField label="Текущий пароль" placeholder="Адрес электронной почты" ></TextField>
                <TextField label="Новый пароль" placeholder="Номер телефона" ></TextField>
                <TextField label="Подтверждение пароля" placeholder="Подтверждение пароля" ></TextField>
            </div>
            <Button classname="password-modal__button" onClick={()=>{}}>Сохранить</Button>
        </div>
    </AppModal>
}

export default PasswordModal