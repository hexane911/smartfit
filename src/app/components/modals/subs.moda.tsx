import Button from "../button";
import AppModal from "../modal";
import './logout.modal.sass'
import circle from '../../assets/images/warning.svg'
import { IModal } from "./modal.interface";


const SubsModal = ({isOpen, action} : IModal) => {
  return (
    <AppModal isOpen={isOpen}>
      <div className="logout-modal">
        <img src={circle} alt="" className="logout-modal__img" />
        <h2 className="modal-title logout-modal__title">Подписка истекла</h2>
        <p className="logout-modal__sub">
        Вы хотите продлить свою подписку?
        </p>
        <div className="logout-modal__box">
          <Button classname="logout-modal__button" onClick={action} clear>
          продлить
          </Button>
          <Button classname="logout-modal__button" onClick={() => {}} red>
            ВЫЙТИ
          </Button>
        </div>
      </div>
    </AppModal>
  );
};

export default SubsModal;
