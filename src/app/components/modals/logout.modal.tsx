import Button from "../button";
import AppModal from "../modal";
import './logout.modal.sass'
import circle from '../../assets/images/warning.svg'
import { IModal } from "./modal.interface";


const LogoutModal = ({isOpen, action} : IModal) => {
  return (
    <AppModal isOpen={isOpen}>
      <div className="logout-modal">
        <img src={circle} alt="" className="logout-modal__img" />
        <h2 className="modal-title logout-modal__title">Вы уверены?</h2>
        <p className="logout-modal__sub">
          Что хотите выйти из личного кабинета
        </p>
        <div className="logout-modal__box">
          <Button classname="logout-modal__button" onClick={action} clear>
            ОТМЕНИТЬ
          </Button>
          <Button classname="logout-modal__button" onClick={() => {}} red>
            ВЫЙТИ
          </Button>
        </div>
      </div>
    </AppModal>
  );
};

export default LogoutModal;
