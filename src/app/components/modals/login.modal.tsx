import Button from "../button"
import AppModal from "../modal"
import TextField from "../text-field"
import './login.modal.sass'
import { IModal } from "./modal.interface"


const LoginModal = ({isOpen, action} : IModal) => {
    return <AppModal isOpen={isOpen} closeButton={{
        show: true,
        onClick: action
    }}>
        <div className="login-modal">
            <div className="login-modal__top">
                <h2 className="modal-title">
                    Вход в аккаунт
                </h2>
            </div>
            <div className="login-modal__content">
                <TextField label="Ваш логин" placeholder="Адрес электронной почты"></TextField>
                <TextField label="Ваш пароль" placeholder="Пароль"></TextField>
            </div>
            <Button onClick={()=>{}} classname="login-modal__button">Войти в аккаунт</Button>
            <p className="login-modal__bottom">У вас нет аккаунта? <a href="" className="login-modal__link">Создать</a></p>
        </div>
    </AppModal>
}

export default LoginModal