import "./scrolling-content.sass";
import sampleVideo from "../assets/images/sample-video.png";
import rewindIcon from "../assets/images/rewind.svg";
import fireIcon from "../assets/images/fire.svg";
import { useEffect, useRef, useState } from "react";

interface IScrolling {
  small?: boolean;
  className?: string;
}

const ScrolligContent = ({ small, className }: IScrolling) => {
//   var scroll = document.getElementById("scrolling__bar");

//   scroll!.oninput = function () {
//     var panel = document.getElementById("scrolling__content");

//     var total = panel!.scrollWidth - panel!.offsetWidth;
//     //@ts-ignore
//     var percentage = total * (this.value / 100);

//     console.log(total);
//     panel!.scrollLeft = percentage;
//     //console.log(percentage);
//   };

const scrollRef = useRef<HTMLInputElement>(null)
const containerRef = useRef<HTMLDivElement>(null)
const [value, setValue] = useState(0)
useEffect(()=>{
    if(scrollRef.current && containerRef.current) {
        const total = containerRef.current.scrollWidth - containerRef.current.offsetWidth
        containerRef.current.scrollLeft = total * (value / 100 )

    }
}, [value])

  return (
    <div className={`scrolling ${className ? className : ""} ${small ? 'small' : ''}`}>
      <input type="range" className="scrolling__bar" defaultValue={0} onChange={(e)=>{setValue(+e.target.value)}} id={'scrolling__bar'}  ref={scrollRef}/>
      <div className="scrolling__content" ref={containerRef} id='scrolling__content'>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
        <div className="scrolling__item">
          <p className="scrolling__number">1/30</p>
          <div
            className="scrolling__frame"
            style={{
              backgroundImage: `url(${sampleVideo})`,
            }}
          >
            <p className="scrolling__time">29.18</p>
            <img src={rewindIcon} alt="" className="scrolling__icon" />
          </div>
          <p className="scrolling__burn">
            <img src={fireIcon} alt="" className="scrolling__fire" />
            520 ккал
          </p>
        </div>
      </div>
    </div>
  );
};

export default ScrolligContent;
