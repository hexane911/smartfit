import { Swiper, SwiperProps, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "./carousel.sass";

type Props = {
  children: any;
  className: string;
  arrowsDown?: boolean;
  noArrows? :boolean
};

const Carousel = ({ className, arrowsDown, children, noArrows }: Props) => {
  const config: SwiperProps = {
    centeredSlides: false,
    loop: false,
    slidesPerView: "auto",
    spaceBetween: 40,
    navigation: {
      nextEl: `.${className}__next`,
      prevEl: `.${className}__prev`,
    },
    modules: [Navigation]
  };
  return (
    <div className={`carousel ${className ? className : ""}`}>
        <div className="carousel__inner">
            <Swiper className="carousel__swiper" {...config}>
                {children!.map((el: any) => {
                return <SwiperSlide>{el}</SwiperSlide>;
                })}
            </Swiper>

        </div>
      {arrowsDown && !noArrows && (
        <div className={`carousel__arrows`}>
          <div
            className={`carousel__arrow-prev carouser__arrow ${className}__prev`}
          ></div>
          <div
            className={`carousel__arrow-next carouser__arrow ${className}__next`}
          ></div>
        </div>
      )}
      {!arrowsDown && !noArrows && (
        <div
          className={`carousel__arrow-prev carouser__arrow ${className}__prev absolute`}
        ></div>
      )}
      {!arrowsDown && !noArrows && (
        <div
          className={`carousel__arrow-next carouser__arrow ${className}__next absolute`}
        ></div>
      )}
    </div>
  );
};

export default Carousel;

//${!arrowsDown ? 'absolute' : ''}
