import "./footer.sass";
import { ReactComponent as Logo } from "../assets/images/logo-gray.svg";
import telegram from "../assets/images/telegram.svg";
import vk from "../assets/images/vk.svg";
import youtube from "../assets/images/youtube.svg";
import instagram from "../assets/images/instagram.svg";
import { Link } from "react-router-dom";

import mir from "../assets/images/payment/mir.svg";
import maestro from "../assets/images/payment/maestro.svg";
import visa from "../assets/images/payment/visa.svg";
import mc from "../assets/images/payment/mc.svg";
import alpha from "../assets/images/payment/alpha.svg";
import paypal from "../assets/images/payment/pay-pal.svg";
import google from "../assets/images/payment/google.svg";
import apple from "../assets/images/payment/apple.svg";
import tele2 from "../assets/images/payment/tele2.svg";
import yandex from "../assets/images/payment/yandex.svg";
import mts from "../assets/images/payment/mts.svg";
import beeline from "../assets/images/payment/beeline.svg";
import qiwi from "../assets/images/payment/qiwi.svg";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__main">
        <div className="wrapper footer__inner">
          <div className="footer__links">
            <Link className="footer__link" to={"/"}>
              Тренеры
            </Link>
            <Link className="footer__link" to={"/"}>
              Сообщество
            </Link>
            <a className="footer__link">Соглашение</a>
            <Link className="footer__link" to={"/"}>
              Программы
            </Link>
            <Link className="footer__link" to={"/"}>
              Тарифы
            </Link>
            <a className="footer__link">Обработка данных</a>
            <Link className="footer__link" to={"/"}>
              Блог
            </Link>
            <a className="footer__link">Тренерам</a>
            <a className="footer__link">Свяжитесь с нами</a>
            <a className="footer__link"></a>
          </div>
          <div className="footer__payment">
            <p className="footer__link n">Способы оплаты на сайте:</p>
            <div className="footer__payment-row">
              <img src={mir} alt="" className="footer__payment-icon" />
              <img src={visa} alt="" className="footer__payment-icon" />
              <img src={mc} alt="" className="footer__payment-icon" />
              <img src={maestro} alt="" className="footer__payment-icon" />
              <img src={alpha} alt="" className="footer__payment-icon" />
              <img src={paypal} alt="" className="footer__payment-icon" />
            </div>
            <div className="footer__payment-row second">
              <img src={google} alt="" className="footer__payment-icon" />
              <img src={apple} alt="" className="footer__payment-icon" />
              <img src={tele2} alt="" className="footer__payment-icon" />
              <img src={yandex} alt="" className="footer__payment-icon" />
              <img src={mts} alt="" className="footer__payment-icon" />
              <img src={beeline} alt="" className="footer__payment-icon" />
              <img src={qiwi} alt="" className="footer__payment-icon" />
            </div>
          </div>
          <div className="footer__misc">
            <Link className="footer__logo-link" to="/">
              <Logo className="footer__logo" />
            </Link>
            <div className="footer__socials">
              <a href="" className="footer__social">
                <img src={instagram} alt="" className="footer__social-icon" />
              </a>
              <a href="" className="footer__social">
                <img src={youtube} alt="" className="footer__social-icon" />
              </a>
              <a href="" className="footer__social">
                <img src={vk} alt="" className="footer__social-icon" />
              </a>
              <a href="" className="footer__social">
                <img src={telegram} alt="" className="footer__social-icon" />
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="footer__creds">© 2022 SMARTFIT. Все права защищены</div>
    </footer>
  );
};

export default Footer;
