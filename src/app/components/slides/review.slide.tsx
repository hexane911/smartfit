import { Review } from "../../types/review";
import './review.slide.sass'

interface IReviewSlide extends Review {
    className?: string 
}

const ReviewSlide = ({image, name, text, className} : IReviewSlide) => {
    return <div className={`review-slide ${className ? className : ''}`}>
        <div className="review-slide__frame" style={{
            backgroundImage: `url(${image})`
        }}/>
        <div className="review-slide__info">
            <p className="review-slide__name">{name}</p>
            <p className="review-slide__text _16">{text}</p>
        </div>
    </div>
}

export default ReviewSlide