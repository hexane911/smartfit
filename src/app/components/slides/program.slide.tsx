import { Program } from "../../types/program.type";
import timeIcon from '../../assets/images/time.svg'
import videosIcon from '../../assets/images/videos.svg'
import caloriesIcon from '../../assets/images/calories.svg'
import "./program.slide.sass";

const ProgramSlide = ({
  calories,
  time,
  videos,
  image,
  name,
}: Program) => {
  return (
    <div className="program-slide">
      <div
        className="program-slide__frame"
        style={{
          backgroundImage: `url(${image})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
      </div>
      <div className="program-slide__bottom">
        <p className="program-slide__name">{name}</p>
        <div className="program-slide__info">
          <div className="program-slide__item">
            <img src={videosIcon} alt="" className="program-slide__item-icon" />
            {videos}
          </div>
          <div className="program-slide__item">
            <img src={timeIcon} alt="" className="program-slide__item-icon" />
            {time}
          </div>
          <div className="program-slide__item">
            <img src={caloriesIcon} alt="" className="program-slide__item-icon" />
            {calories}
          </div>
        </div>
      </div>
    </div>
  );
};


export default ProgramSlide