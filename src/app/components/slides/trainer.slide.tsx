import { Trainer } from "../../types/trainer";
import videosIcon from '../../assets/images/videos.svg'
import appsIcon from '../../assets/images/apps.svg'
import userIcon from '../../assets/images/user.svg'
import "./trainer.slide.sass";

const TrainerSlide = ({ image, name, people, programs, videos }: Trainer) => {
  return (
    <div className="trainer-slide">
      <div
        className="trainer-slide__frame"
        style={{
          backgroundImage: `url(${image})`,
        }}
      ></div>
      <div className="trainer-slide__info">
        <p className="trainer-slide__name">{name}</p>
        <div className="trainer-slide__misc">
          <div className="trainer-slide__item">
            <img src={videosIcon} alt="" className="trainer-slide__item-icon" />
            {videos}
          </div>
          <div className="trainer-slide__item">
            <img src={appsIcon} alt="" className="trainer-slide__item-icon" />
            {programs}
          </div>
          <div className="trainer-slide__item">
            <img src={userIcon} alt="" className="trainer-slide__item-icon" />
            {people}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TrainerSlide;
