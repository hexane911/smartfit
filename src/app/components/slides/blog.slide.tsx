import { Blog } from '../../types/blog'
import likeIcon from '../../assets/images/like.svg'
import './blog.slide.sass'


interface BlogSlide extends Blog {
    className?: string
}

const BlogSlide = ({date, image, likes, title, className} : BlogSlide) => {
    return <div className={`blog-slide ${className ? className : ''}`}>
        <div className="blog-slide__frame"
            style={{
                backgroundImage: `url(${image})`
            }}
        ></div>
        <div className="blog-slide__info">
            <p className="blog-slide__title">{title}</p>
            <div className="blog-slide__misc">
                <p className="blog-slide__date">{date}</p>
                <p className="blog-slide__time"></p>
                <p className="blog-slide__likes">
                    <img src={likeIcon} alt="" className="blog-slide__heart" />
                    {likes}
                </p>
            </div>
        </div>
    </div>
}

export default BlogSlide