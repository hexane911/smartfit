import "./text-field.sass";

type Props = {
  placeholder: string;
  label: string;
  className: string;
};

const TextArea = ({ placeholder, label, className }: Props) => {
  return (
    <label className="text-field__label">
      {label}
      <div className="text-field__box">
        <textarea
          className={`text-field__area text-field ${className ? className : ""}`}
          placeholder={placeholder || ""}
        ></textarea>
      </div>
    </label>
  );
};

export default TextArea;
