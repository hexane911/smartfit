import Modal from "react-modal";
import "./modal.sass";
import cross from "../assets/images/modal-cross.svg";
import { useWindowSize } from "../hooks";
import { useEffect, useRef, useState } from "react";

type Props = {
  children?: any;
  isOpen: boolean;
  closeButton?: {
    show: boolean;
    onClick: any;
  };
};

const AppModal = ({ children, isOpen, closeButton }: Props) => {
  Modal.setAppElement("#root");
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };
  const MobileStyles = {
    content: {
      left: '0%',
      top: '0%',
      transform: 'none',
      margin: '0',
      paddingTop: '86px'
    },
  }


  const {width} = useWindowSize()


  


  return (
    <Modal isOpen={isOpen} style={(width > 419 ? customStyles : MobileStyles)}>
      
      <div className="modal-content" >
      {closeButton?.show && (
        <div className="modal-close" onClick={closeButton.onClick}>
          <img src={cross} alt="" className="modal-close-img" />
        </div>
      )}
        {children}
      </div>
    </Modal>
  );
};

export default AppModal;
