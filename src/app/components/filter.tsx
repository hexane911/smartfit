import Button from "./button";
import "./filter.sass";

type Props = {
  options: string[];
  handler?: any; //must be a state setter function
  className?: string;
  center?: boolean;
  allButton?: boolean;
  current?: string; //element of options array
  gap?: number;
  deselectedState?: string; //element of options array
};

const Filter = ({
  handler,
  options,
  allButton,
  center,
  className,
  current,
  deselectedState,
  gap,
}: Props) => {
  return (
    <div className={`
        filter 
        ${className ? className : ''} 
        ${center ? 'center' : ''}
    `}>
      {options.map((el) => {
        return (
          <Button
            clear={!(current == el)}
            onClick={() => {
              el !== current ? handler(el) : handler(deselectedState || '');
            }}
            classname={`filter__button`}
          >
            {el}
          </Button>
        );
      })}
    </div>
  );
};

export default Filter;
