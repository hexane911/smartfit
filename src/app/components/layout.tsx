import Footer from './footer'
import Header from './header'
import './layout.sass'
import Pages from './pages'


const Layout  = () => {
    return <div className="layout">
        <Pages/>
    </div>
}

export default Layout