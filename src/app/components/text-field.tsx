import Button from "./button";
import "./text-field.sass";

type Props = {
  type?: string;
  className?: string;
  placeholder?: string;
  label?: string;
  button?: {
    text: string;
    onClick: any;
  };
};

const TextField = ({ type, className, placeholder, label, button }: Props) => {
  const buttonEl = (
    <Button classname="text-field__button" onClick={button?.onClick}>
      {button?.text}
    </Button>
  );
  return (
    <>
      {!!label ? (
        <label className="text-field__label">
          {label}
          <div className="text-field__box">
            <input
              type={type || "text"}
              className={`text-field ${className ? className : ""}`}
              placeholder={placeholder || ""}
            ></input>
            {!!button && buttonEl}
          </div>
        </label>
      ) : (
        <div className="text-field__box">
          <input
            type={type || "text"}
            className={`text-field ${className ? className : ""}`}
            placeholder={placeholder || ""}
          ></input>
        </div>
      )}
    </>
  );
};

export default TextField;
