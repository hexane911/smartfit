import Button from "./button";
import { ReactComponent as Logo } from "../assets/images/logo.svg";
import { Link, useLocation } from "react-router-dom";
import "./header.sass";
import { User } from "../types/user";
import LoginModal from "./modals/login.modal";
import { useState } from "react";

type Props = {
  user?: User;
};

const Header = () => {
  const location = useLocation();
  const inProfile = location.pathname.includes("profile");
  const loggedIn = false;

  const [modalOpen, setModalOpen] = useState(false);
  const [mobileOpen, setMobileOpen] = useState(false);

  return (
    <header className={`header ${inProfile ? "logged" : ""}`}>
      <LoginModal isOpen={modalOpen} action={() => setModalOpen(false)} />
      <div className="header__container wrapper">
        <Link to="/" onClick={() => {setMobileOpen(false)}} className="header__logo-link">
          <Logo className="header__logo" />
        </Link>
        <div className="header__menu">
          <Link
            to={"/trainers"}
            className={`header__item ${
              location.pathname === "/trainers" ? "active" : ""
            }`}
          >
            Тренеры
          </Link>
          <Link
            to={"/programs"}
            className={`header__item ${
              location.pathname === "/programs" ? "active" : ""
            }`}
          >
            Программы
          </Link>
          <Link
            to={"/blog"}
            className={`header__item ${
              location.pathname === "/blog" ? "active" : ""
            }`}
          >
            Блог
          </Link>
          <Link
            to={"/community"}
            className={`header__item ${
              location.pathname === "/community" ? "active" : ""
            }`}
          >
            Сообщество
          </Link>
        </div>
        {loggedIn ? (
          <Link to="/profile" className="header__profile">
            Dima.vasiliev.98!
            <div className="header__avatar">d</div>
          </Link>
        ) : (
          <div className="header__buttons">
            <Button icon="bolt" onClick={() => {}} classname="header__button">
              <Link to={"./purchase"}>Тренироваться</Link>
            </Button>
            <Button
            icon="login"
              clear
              onClick={() => {
                setModalOpen(true);
              }}
              classname="header__button"
            >
              Войти
            </Button>
          </div>
        )}
        <div
          className={`header-mobile__button ${
            mobileOpen ? "opened" : "hidden"
          }`}
          onClick={() => setMobileOpen((s) => !s)}
        >
          <div className="header-mobile__line line"></div>
        </div>
        <div
          className={`header-mobile__menu ${mobileOpen ? "opened" : "hidden"}`}
        >
          <Link onClick={() => {setMobileOpen(false)}}
            to={"/trainers"}
            className={`header__item ${
              location.pathname === "/trainers" ? "active" : ""
            }`}
          >
            Тренеры
          </Link>
          <Link onClick={() => {setMobileOpen(false)}}
            to={"/programs"}
            className={`header__item ${
              location.pathname === "/programs" ? "active" : ""
            }`}
          >
            Программы
          </Link>
          <Link onClick={() => {setMobileOpen(false)}}
            to={"/blog"}
            className={`header__item ${
              location.pathname === "/blog" ? "active" : ""
            }`}
          >
            Блог
          </Link>
          <Link onClick={() => {setMobileOpen(false)}}
            to={"/community"}
            className={`header__item ${
              location.pathname === "/community" ? "active" : ""
            }`}
          >
            Сообщество
          </Link>
          {loggedIn ? (
            <Link onClick={() => {setMobileOpen(false)}} to="/profile" className="header__profile">
              Dima.vasiliev.98!
              <div className="header__avatar">d</div>
            </Link>
          ) : (
            <div className="header-mobile__buttons">
              <Button onClick={() => {setMobileOpen(false)}} classname="header__button">
                <Link to={"./purchase"}>Тренироваться</Link>
              </Button>
              <Button
                clear
                onClick={() => {
                  setModalOpen(true);
                  setMobileOpen(false)
                }}
                classname="header__button last"
              >
                Войти
              </Button>
            </div>
          )}
        </div>
      </div>
    </header>
  );
};

export default Header;
