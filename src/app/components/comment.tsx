import './comment.sass'
import example from '../../app/assets/images/comment-avatae.png'


const Comment = () => {
    return (
        <div className="comment">
            <div className="comment__top">
                <img src={example} alt="" className="comment__image" />
                <div className="comment__misc">
                    <p className="comment__name">Дмитрий</p>
                    <p className="comment__time">3 месяца назад</p>
                </div>
            </div>
            <div className="comment__text">
            Спасибо большое за эту статью, отличные рецепты, использую их каждый день. Очень вкусно и полезно, всем рекомендую попробовать хотя бы раз.
            </div>
        </div>
    )
}

export default Comment