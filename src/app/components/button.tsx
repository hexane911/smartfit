import "./button.sass";
import bolt from "../assets/images/bolt.svg";
import login from "../assets/images/login-icon.svg";
import search from "../assets/images/search.svg";
import video from "../assets/images/play-circle.svg";

type Props = {
  onClick(): void;
  children: any;
  clear?: boolean;
  classname?: string;
  red?: boolean;
  icon?: "bolt" | "login" | "search" | "video";
};
const Button = ({ onClick, children, clear, classname, red, icon }: Props) => {
  let currentIcon;
  switch (icon) {
    case "bolt":
      currentIcon = bolt;
      break;
    case "login":
      currentIcon = login;
      break;
    case "search":
      currentIcon = search;
      break;
    case "video":
      currentIcon = video
      break
  }

  return (
    <button
      onClick={onClick}
      className={`button ${clear ? "_clear" : ""} ${classname || ""} ${
        red ? "_red" : ""
      }`}
    >
      <div className="button__inner">
        {children}
        {icon && <img src={currentIcon} alt="" className="button__icon" />}
      </div>
    </button>
  );
};

export default Button;
