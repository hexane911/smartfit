import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import BlogPage from "../pages/blog.page";
import ProfileIndex from "../pages/lk";
import LK_Programs from "../pages/lk/lk-programs";
import Personal from "../pages/lk/personal";
import Profile from "../pages/lk/profile";
import ProgramPage from "../pages/program.page";
import Progress from "../pages/lk/progress";
import Subscription from "../pages/lk/subscription";
import MainPage from "../pages/main.page";
import ProgramsPage from "../pages/programs.page";
import PurchasePage from "../pages/purchase.page";
import TrainerPage from "../pages/trainer.page";
import TrainersPage from "../pages/trainers.page";
import Footer from "./footer";
import Header from "./header";
import AppModal from "./modal";
import LoginModal from "./modals/login.modal";
import PostPage from "../pages/post.page";
import Trainings from "../pages/lk/trainings";

function Pages() {
  return (
    <Router>
      <Header />

      <Routes>
        <Route path="/" element={<MainPage/>}></Route>
        <Route path="/programs" element={<ProgramsPage/>}></Route>
        <Route path="/trainers" element={<TrainersPage />}></Route>
        <Route path="/trainer" element={<TrainerPage />}></Route>
        <Route path="/program" element={<ProgramPage />}></Route>
        <Route path="/blog" element={<BlogPage/>}></Route>
        <Route path="/blog-page" element={<PostPage />} />
        <Route path="/purchase" element={<PurchasePage />}></Route>
        <Route path="/profile" element={<Profile />}>
          <Route index element={<ProfileIndex />} />
          <Route path="personal" element={<Personal />} />
          <Route path="progress" element={<Progress />} />
          <Route path="subscription" element={<Subscription />} />
          <Route path="programs" element={<LK_Programs/>} />
          <Route path="trainings" element={<Trainings/>} />
        </Route>
      </Routes>
      <Footer />
    </Router>
  );
}

export default Pages;
